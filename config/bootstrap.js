//var fs = require('fs')
//  , path = require('path');
//
//module.exports.bootstrap = function (cb) {
//  // Whatever else you want to bootstrap...
//
//  var postsSource = path.join(process.cwd(), 'uploads')
//    , postsDest = path.join(process.cwd(), '.tmp/public/uploads');
//
//  fs.symlink(postsSource, postsDest, function(err) {
//    cb(err);
//  });
//};

module.exports.bootstrap = function(cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};
