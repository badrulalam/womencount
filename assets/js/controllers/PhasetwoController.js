angular.module('app')



//.controller('PhasetwoController', function ($scope, $filter, $document, $location, $anchorScroll, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {

            .controller('PhasetwoController', function ($scope,$rootScope,$modal,$translate, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {





                    $scope.gender_gap_open = true;
        $scope.new_voter_open = false;



        var map_path = "json/all-sub-dist-all-phase2.geojson";


        function countryClick(featureSelected, leafletEvent)
        {



                var layer = leafletEvent.target;
                layer.setStyle({
                        weight: 2,
                        color: '#666',
                        fillColor: 'black'
                });
                var popupContent = getPCONT(layer, featureSelected);
                layer.bindPopup(popupContent).openPopup();

        }




        var MouseOverStyle =
        {
                fillColor: 'white',
                weight: 3,
        };



        $scope.showme = false;
        var polygons = [];
        var all_features = [];
        var cl =false;
        var cl_color;



        function collectMe(feature, layer)
        {

                var defstyle;
                polygons[feature.properties.id_3] = layer;
                all_features[feature.properties.id_3] = feature;



                layer.on("click", function (e) { console.log("onclick console", feature);
                        $scope.showme = false;
                        var popupContent = getPCONT(feature);
                        layer.bindPopup(popupContent).openPopup();

                });

        }



        function toMap() {
                var duration = 700; //milliseconds
                var offset = 30;
                var map = angular.element(document.getElementById('map'));
                $document.scrollToElementAnimated(map, offset, duration);


        }

        $scope.selectMe = function (id) {

                toMap();

                if(cl){
                        var MouseOutStyle = {
                                weight: 1,
                                fillColor: cl_color
                        };

                        cl.setStyle(MouseOutStyle);
                }

                $location.hash('map');

                //  $anchorScroll();

                var l = polygons[id];
                l.setStyle({
                        fillColor: 'black'

                });
                cl = l;
                cl_color = getColor(all_features[id].properties.phase_2);

                var popupContent = getPCONT(all_features[id]);
                l.bindPopup(popupContent).openPopup();

        };



        function getPCONT(featureSelected)
        {
                var cont = "<table class='table'><tr><td>District</td><td>" + featureSelected.properties.district + "</td></tr><tr><td>Upazila</td><td>" + featureSelected.properties.upazila + "</td></tr></table>";
                return cont;

        }

        angular.extend($scope, {
                bd: {
                        lat: 23.7000,
                        lng: 90.3500,
                        zoom: 7
                },
                defaults: {
                        scrollWheelZoom: true,
                        attributionControl: false
                },
                layers: {
                        baselayers: {
                                img: {
                                    name: 'img',

                                    url: '/images/be.png',

                                    type: 'xyz'
                                },

                                //osm: {
                                //        name: 'osm',
                                //        url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                                //        type: 'xyz'
                                //}

                        },
                        overlays: {
                                wms: {
                                        name: 'Bangladesh',
                                        type: 'wms',
                                        visible: true,
                                        url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                                        layerParams: {
                                                service: 'WMS',
                                                version: '1.1.0',
                                                request: 'GetMap',
                                                srs: 'EPSG:4326',
                                                style: 'polygon',
                                                layers: 'distr-map-voter-update-data',
                                                format: 'image/png',
                                                transparent: true
                                        }
                                }
                        }
                }



        });



        function countryMouseover(feature, leafletEvent) {
                var layer = leafletEvent.target;
                layer.setStyle({
                        fillColor: 'white'
                });
                layer.bringToFront();
                $scope.name = feature.properties.name;
                $scope.gap = feature.properties.m_f_diff;

                $scope.femper = feature.properties.female_per;

                $scope.maleper = feature.properties.male_per;


                console.log(feature);
        }


        function getColor(code) {

                var nc = Math.floor(code);
                if (nc == 1)
                {
                        //return "#FE8484";
                        return "#EB7F00";
                }

                if (nc == 2)
                {
                        return "#248DA1";
                }

                if (nc== 3)
                {
                        return "#4CAC3B";
                }
                //
                //
                //if (nc >= 15 && nc < 20)
                //{
                //    return "#FE2020";
                //}

                //if (nc >= 20 && nc < 25)
                //{
                //    return "#DB0000";
                //}
                //
                //if (nc >= 25 && nc < 30)
                //{
                //    return "#BA0000";
                //}
                //
                //if (nc >= 30 && nc < 35)
                //{
                //    return "#6B0000";
                //}
                //
                //if (nc >= 35 && nc < 40)
                //{
                //    return "#530000";
                //}
                //
                //if (nc >= 40)
                //{
                //    return "#250000";
                //}
        }

        function resetHighlight(e) {
                geojson.resetStyle(e.target);
        }
        function style(feature)
        {
                return {
                        fillColor: getColor(feature.properties.phase_2),
                        //fillColor: '#eaeaea',
                        weight: 1,
                        opacity: 1,
                        color: '#969596',

                        //  dashArray: '3',
                        fillOpacity: 0.7,

                };
        }


        $http.get(map_path).success(function (data, status) {
                angular.extend($scope, {
                        geojson: {
                                data: data,
                                style: style,
                                mouseout: resetHighlight,
                                resetStyleOnMouseout: true,
                                onEachFeature: collectMe,


                        }
                });
                $scope.features = data.properties;
        });




        //var phase_map  =  "assets/json/af-phase-map-one.geojson";


        //$http.get(map_path).success(function (data1, status) {
        //    angular.extend($scope, {
        //        geojson: {
        //            data: data1,
        //
        //            selectedCountry: {},
        //            onEachFeature: collectMe,
        //            // filter: filterLayer,
        //            pointToLayer: plotLayer,
        //            style: style,
        //        }
        //    });
        //    $scope.features1 = data1.properties;
        //});



        function plotLayer(feature, latlng){
                return L.circleMarker(latlng, {
                        radius: 8,
                        fillColor: "#FE3939",
                        color: "#000",
                        weight: 1,
                        opacity: 1,
                        fillOpacity: 1,
                        //zIndex: 100
                });
        };


        //var map_label2 = "assets/json/phase2-label.json";
        //
        //$http.get(map_label2).success(function (data3, status) {
        //    angular.extend($scope, {
        //        markers: data3,
        //    });
        //});







});

