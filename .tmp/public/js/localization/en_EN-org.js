// Configuring $translateProvider
app.config(['$translateProvider', function ($translateProvider) {

  $translateProvider.translations('en_EN', {



      //Home  PAGE
    'BUTTON_LANG_BN':'BN',
    'BUTTON_LANG_EN':'EN',
    'women_logo': 'images/women-logo-isolated-en.gif',
    'count_logo':'images/count-isolated.png',

    '558,103 Women were missing from the last voter list':'558,103 Women were missing from the last draft supplementary voter list',
     'Scroll-over-your-district':'Scroll over your district in the map to find out the number of missing women from the voters roll in 2014',
     'District':'District',

      'missing-from-role': 'Women were missing from the draft supplementary voter list in 2014',
      'Sign-the-pledge': 'Sign the pledge',

       'sign-pl-img':'images/press-statement-english.jpg',

     'pr-statment':'ewg/Press-Statement-BN.pdf',

   
      'Count':'Count',
      'Campaign-resources-btn': 'Get your campaign resources here',
      'Downloads':'Downloads',
      'BANGLA': 'BANGLA',
      'Placard-Red-Logo-bn)':'PDF Placard (Red Bangla)',
      'Placard-Blue-Logo-bn)':'PDF Placard (Blue Bangla)',
      'Logo-Red-bn':'Logo (Red Bangla)',
      'Logo-Blue-bn':'Logo (Blue Bangla)',


   'DownLogo': 'Logo',
   'DownPlacard':'Placard',



      'Document':'Document',
      'Voter-List-Update-Information-EN':'Voter List Update Information(EN)',
      'Voter-List-Update-Information-BN' :'Voter List Update Information (BN)',
     'Fact-Sheet-on-Voter-Registration-Process-EN' :'Fact Sheet on Voter Registration Process (EN)',
     'Fact-Sheet-on-Voter-Registration-Process-BN' :'Fact Sheet on Voter Registration Process (BN)',



      'ENGLISH':'ENGLISH',
      'Placard-Red-Logo-en':'PDF Placard (Red English)',
      'Placard-Blue-Logo-en':'PDF Placard (Blue English)',
      'Logo-Red-en':'Logo (Red English)',
      'Logo-Blue-en':'Logo (Blue English)',
      'copywright':'copyright',
      'c2015': '2015',

      'Barisal':'Barisal',
      'Jhalokathi':'Jhalokathi',
      'Perojpur':'Perojpur',
      'Bhola':'Bhola',
      'Barguna': 'Barguna',
      'Patuakhali': 'Patuakhali',
      'Bandarban': 'Bandarban',
      'Chittagong':'Chittagong',
      'Cox\'s Bazar':'Cox\'s Bazar',
      'Brahmanbaria':'Brahmanbaria',
      'Chandpur': 'Chandpur',
      'Comilla':'Comilla',
      'Khagrachari':'Khagrachari',
      'Feni':'Feni',
      'Laksmipur':'Laksmipur',
      'Noakhali':'Noakhali',
      'Rangamati':'Rangamati',
      'Dhaka':'Dhaka',
      'Gazipur':'Gazipur',
      'Manikganj':'Manikganj',
      'Munshiganj': 'Munshiganj',
      'Narayanganj':'Narayanganj',
      'Norshingdi':'Norshingdi',
      'Faridpur':'Faridpur',
      'Gopalganj':'Gopalganj',
      'Madaripur': 'Madaripur',
      'Shariatpur':'Shariatpur',
      'Jamalpur':'Jamalpur',
      'Sherpur':'Sherpur',
      'Kishoreganj':'Kishoreganj',
      'Mymensingh':'Mymensingh',
      'Netrokona':'Netrokona',
      'Tangail':'Tangail',
      'Jessore':'Jessore',
      'Jhenaidah':'Jhenaidah',
      'Magura':'Magura',
      'Narail':'Narail',
      'Bagerhat':'Bagerhat',
      'Khulna':'Khulna',
      'Satkhira':'Satkhira',
      'Chuadanga':'Chuadanga',
      'Kushtia':'Kushtia',
      'Meherpur': 'Meherpur',
      'Bogra':'Bogra',
      'Joypurhat':'Joypurhat',
      'Dinajpur':'Dinajpur',
      'Panchagarh':'Panchagarh',
      'Thakurgaon':'Thakurgaon',
      'Pabna':'Pabna',
      'Sirajganj':'Sirajganj',
      'Naogaon':'Naogaon',
      'Natore':'Natore',
      'Chapai Nawabganj':'Chapai Nawabganj',
      'Rajshahi':'Rajshahi',
      'Gaibandha':'Gaibandha',
      'Kurigram':'Kurigram',
      'Lalmonirhat':'Lalmonirhat',
      'Nilphamari':'Nilphamari',
      'Rangpur':'Rangpur',
      'Habiganj':'Habiganj',
      'Moulvibazar':'Moulvibazar',
      'Sunamganj':'Sunamganj',
      'Sylhet':'Sylhet',
      'Rajbari':'Rajbari',


   


   //--Miss fem

      '12,467':'12,467',
      '3,413':'3,413',
      '5,590':'5,590',
      '13,572':'13,572',
      '2,091':'2,091',
      '5,564':'5,564',
      '846':'846',
      '47,992':'47,992',
      '12,797':'12,797',
      '22,379':'22,379',
      '25,830':'25,830',
      '46,127':'46,127',
      '1,044':'1,044',
      '17,261':'17,261',
      '18,259':'18,259',
      '31,747':'31,747',
      '1,507':'1,507',
      '6,796':'6,796',
      '7,349':'7,349',
      '8,584':'8,584',
      '10,130':'10,130',
      '10,318':'10,318',
      '13,657':'13,657',
      '8,258':'8,258',
      '5,251':'5,251',
      '5,445':'5,445',
      '7,675':'7,675',
      '5,510':'5,510',
      '1,946':'1,946',
      '13,507':'13,507',
      '19,331':'19,331',
      '7,089':'7,089',
      '19,347':'19,347',
      '6,487':'6,487',
      '4,630':'4,630',
      '2,307':'2,307',
      '2,174':'2,174',
      '4,688':'4,688',
      '2,308':'2,308',
      '3,934':'3,934',
      '2,215':'2,215',
      '4,467':'4,467',
      '3,409':'3,409',
      '5,181':'5,181',
      '2,013':'2,013',
      '5,240':'5,240',
      '1,491':'1,491',
      '2,658':'2,658',
      '7,911':'7,911',
      '8,722':'8,722',
      '6,679':'6,679',
      '3,819':'3,819',
      '9,412':'9,412',
      '4,239':'4,239',
      '2,444':'2,444',
      '4,251':'4,251',
      '2,942':'2,942',
      '4,037':'4,037',
      '3,584':'3,584',
      '10,594':'10,594',
      '10,932':'10,932',
      '8,156':'8,156',
      '20,104':'20,104',
      '2,302':'2,302'





  });

  //$translateProvider.use('en_EN'); // uncomment this if you want to set english as default
  //$translateProvider.rememberLanguage(true);
}]);
