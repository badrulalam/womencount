angular.module('app')
    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider

            .state('home', {
                url: '/',
                //templateUrl: 'templates/home.html'
                templateUrl: 'templates/app_four_gender_gap.html',
                controller: 'AppFourGenderGapController'
            })

            .state('home.pledgcountform', {
                url: 'sign-in-pledge',
                templateUrl: 'templates/pledgcountformr.html',
                controller: 'PledgcountformController'
            })


            .state('home.downloadmeterials', {
                url: 'downloads',
                templateUrl: 'templates/downloads.html',
                controller: 'DownloadsController'
            })

            .state('home.pressconference', {
                url: 'press-conference',
                templateUrl: 'templates/pressconference.html',
                controller: 'PressconferenceController'
            })


            .state('home.phasetwo', {
                url: 'phase-two',
                templateUrl: 'templates/phasetwo.html',
                controller: 'PhasetwoController'
            })

            .state('home.phasethree', {
                url: 'phase-three',
                templateUrl: 'templates/phasethree.html',
                controller: 'PhasethreeController'
            })




            //.state('all_apps', {
            //    url: '/all_apps',
            //    templateUrl: '/templates/all_apps.html',
            //    controller: 'AllAppsController'
            //})
            //.state('about', {
            //    url: '/about',
            //    templateUrl: 'templates/about.html',
            //    controller: 'AboutController'
            //})
            //
            //.state('what_we_do', {
            //    url: '/what_we_do',
            //    templateUrl: 'templates/what_we_do.html',
            //    controller: 'WhatWeDoController'
            //})
            //.state('news', {
            //    url: '/news',
            //    templateUrl: 'templates/news.html',
            //    controller: 'NewsController'
            //})
            //
            //.state('archives', {
            //    url: '/archives',
            //    templateUrl: 'templates/archives.html',
            //    controller: 'ArchivesController'
            //})
            //
            //.state('contact', {
            //    url: '/contact',
            //    templateUrl: 'templates/contact.html',
            //    controller: 'ContactController'
            //})

            //.state('radio', {
            //    url: '/radio',
            //    templateUrl: 'templates/radio.html',
            //    controller: 'RadioController'
            //})
            //
            //.state('video', {
            //    url: '/video',
            //    templateUrl: 'templates/video.html',
            //    controller: 'VideoController'
            //})

            //.state('gallery', {
            //    url: '/gallery',
            //    templateUrl: 'templates/gallery.html',
            //    controller: 'GalleryController'
            //})
            //
            //.state('app_one', {
            //    url: '/app_one',
            //    templateUrl: 'templates/app_one.html',
            //    controller: 'AppOneController'
            //})

            .state('app_four', {
                url: '/app_four',
                templateUrl: 'templates/app_four.html',
                controller: 'AppFourController'
            })
            //.state('app_six', {
            //    url: '/app_six',
            //    templateUrl: '/templates/app_six.html',
            //    controller: 'AppSixController'
            //})
            //.state('app_six_inf', {
            //    url: '/app_six_inf',
            //    templateUrl: '/templates/app_six_inf.html',
            //    controller: 'AppSixInfController'
            //})
            //.state('app_six_vote', {
            //    url: '/app_six_vote',
            //    templateUrl: '/templates/app_six_vote.html',
            //    controller: 'AppSixVoteController'
            //})

            //.state('new_voter', {
            //    url: '/new_voter',
            //    templateUrl: '/templates/app_four_new_voter.html',
            //    controller: 'AppFourNewVoterController'
            //})

            .state('gender_gap', {
                url: '/gender_gap',
                templateUrl: 'templates/app_four_gender_gap.html',
                controller: 'AppFourGenderGapController'
            })


            //.state('app_three', {
            //    url: '/app_three',
            //    templateUrl: '/templates/app_three.html',
            //    controller: 'AppThreeController'
            //})
            //
            //.state('app_one.occurance_voting', {
            //    url: '/occurance_voting',
            //    templateUrl: '/templates/app_one_occurance_voting.html',
            //    controller: 'AppOneOccuranceVotingController'
            //})
            //.state('app_one.occurance_novoting', {
            //    url: '/occurance_novoting',
            //    templateUrl: '/templates/app_one_occurance_novoting.html',
            //    controller: 'AppOneOccuranceNoVotingController'
            //})
            //
            //.state('app_one.results_party', {
            //    url: '/results_party',
            //    templateUrl: '/templates/app_one_results_party.html',
            //    controller: 'AppOneResultsController'
            //})
            //.state('app_one.results_candidates', {
            //    url: '/results_candidates',
            //    templateUrl: '/templates/app_one_results_candidates.html',
            //    controller: 'AppOneResultsCandidatesController'
            //})
            //.state('app_one.results_gender', {
            //    url: '/results_gender',
            //    templateUrl: '/templates/app_one_results_gender.html',
            //    controller: 'AppOneResultsGenderController'
            //})
            //
            //.state('app_one.observation_ps', {
            //    url: '/observation_ps',
            //    templateUrl: '/templates/app_one_observation_ps.html',
            //    controller: 'AppOneObservationPsController'
            //})
            //.state('app_one.observation_const', {
            //    url: '/observation_const',
            //    templateUrl: '/templates/app_one_observation_constituency.html',
            //    controller: 'AppOneObservationConsController'
            //})

            //---Arif Start----

            //.state('app_seven', {
            //    url: '/app_seven',
            //    templateUrl: '/templates/app_seven.html',
            //    controller: 'AppSevenController'
            //})
            //
            //.state('seven_dist_event', {
            //    url: '/district_event',
            //    templateUrl: '/templates/app_seven_district_event.html',
            //    controller: 'AppSevenEventController'
            //})
            //
            //.state('seven_dist_wounded', {
            //    url: '/district_wounded',
            //    templateUrl: '/templates/app_seven_district_wounded.html',
            //    controller: 'AppSevenDistrictWoundedController'
            //})
            //
            //.state('seven_dist_casualties', {
            //    url: '/district_casualties',
            //    templateUrl: '/templates/app_seven_district_casualties.html',
            //    controller: 'AppSevenDistrictCasualtiesController'
            //})


            //.state('app_eight', {
            //    url: '/app_eight',
            //    templateUrl: '/templates/app_eight.html',
            //    controller: 'AppSixController'
            //})
            //
            //.state('app_eight_winer', {
            //    url: '/app_eight_winer',
            //    templateUrl: '/templates/app_eight_winer.html',
            //    controller: 'AppEightWinerController'
            //})
            //
            //
            //.state('app_eight_win_inf', {
            //    url: '/app_eight_win_inf',
            //    templateUrl: '/templates/app_eight_win_inf.html',
            //    controller: 'AppEightWinInfController'
            //})

            //--Arif End------


        ;

        $urlRouterProvider.otherwise('/');


    });