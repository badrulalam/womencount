angular.module('app')

    .controller('CarouselDemoCtrl', function ($scope) {
        $scope.myInterval = 5000;
        var slides = $scope.slides = [
            {image:'slide/images/slide-1.jpg'},
            {image:'slide/images/slide-2.jpg'},
            {image:'slide/images/slide-3.jpg'},
            {image:'slide/images/slide-4.jpg'}


        ];
        //$scope.addSlide = function() {
        //  var newWidth = 600 + slides.length + 1;
        //  slides.push({
        //    image: 'http://placekitten.com/' + newWidth + '/300',
        //    text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
        //    ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
        //  });
        //};

    })


    //.controller('CarouselDemoCtrl', function ($scope) {
    //    $scope.myInterval = 5005;
    //    var slides = $scope.slides = [];
    //    $scope.addSlide = function() {
    //        var newWidth = 600 + slides.length + 1;
    //        slides.push({
    //            image: 'http://placekitten.com/' + newWidth + '/300',
    //            text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
    //            ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
    //        });
    //    };
    //    for (var i=0; i<4; i++) {
    //        $scope.addSlide();
    //    }
    //})







    .controller('AppFourGenderGapController', function ($scope,$rootScope,$modal,$translate, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {




        $scope.gender_gap_open = true;
        $scope.new_voter_open = false;
        $scope.women_logo ='images/women-logo-isolated.gif';

        //$http.get('http://128.199.120.151:1337/district').success(function (data) {
        //
        //    $scope.tableParams = new ngTableParams({
        //        page: 1, // show first page
        //        count: 100, // count per page
        //        sorting: {
        //            name: 'asc'     // initial sorting
        //        }
        //    }, {
        //        total: data.length, // length of data
        //        getData: function ($defer, params) {
        //            // use build-in angular filter
        //            var orderedData = params.sorting() ?
        //                    $filter('orderBy')(data, params.orderBy()) :
        //                    data;
        //
        //            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        //        }
        //    });
        //
        //});





//var map_path ="http://128.199.120.151:8080/geoserver/ewg/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ewg%3Adistr-map-voter-update-data&maxfeatures=350&outputformat=json";
        var map_path = "json/update-women-count.json";



        function countryClick(featureSelected, leafletEvent)
        {



            var layer = leafletEvent.target;
            layer.setStyle({
                weight: 2,
                color: '#666',
                fillColor: 'black'
            });
            var popupContent = getPCONT(layer, featureSelected);
            layer.bindPopup(popupContent).openPopup();

        }




        var MouseOverStyle =
        {
            fillColor: 'white',
            weight: 3,
        };



        $scope.showme = false;
        var polygons = [];
        var all_features = [];
        var cl =false;
        var cl_color;



        function collectMe(feature, layer)
        {

            var defstyle;

            polygons[feature.properties.id_3] = layer;
            all_features[feature.properties.id_3] = feature;

            layer.on("mouseout", function (e) {
                $scope.showme = true;

                var MouseOutStyle = {
                    weight: 1,
                    fillColor: getColor(feature.properties.m_f_diff)
                };

                layer.setStyle(MouseOutStyle);

            });

            layer.on("mouseover", function (e) {
                $scope.showme = true;
                $scope.showdfme = true;

                $scope.name = feature.properties.name;
                $scope.gap = feature.properties.m_f_diff;
                $scope.feminc = feature.properties.female_inc;
                $scope.femper = feature.properties.female_per;
                $scope.maleinc = feature.properties.male_inc_n;
                $scope.maleper = feature.properties.male_per;
                $scope.missfem = feature.properties.miss_fem;


                layer.setStyle(MouseOverStyle);

            });


            layer.on("click", function (e) {
                $scope.showme = false;
                var popupContent = getPCONT(feature);
                layer.bindPopup(popupContent).openPopup();

            });



        }



        function toMap() {
            var duration = 700; //milliseconds
            var offset = 30;
            var map = angular.element(document.getElementById('map'));
            $document.scrollToElementAnimated(map, offset, duration);


        }

        $scope.selectMe = function (id) {

            toMap();

            if(cl){
                var MouseOutStyle = {
                    weight: 1,
                    fillColor: cl_color
                };

                cl.setStyle(MouseOutStyle);
            }

            $location.hash('map');

            //  $anchorScroll();

            var l = polygons[id];
            l.setStyle({
                fillColor: 'black'

            });
            cl = l;
            cl_color = getColor(all_features[id].properties.m_f_diff);

            var popupContent = getPCONT(all_features[id]);
            l.bindPopup(popupContent).openPopup();

        };




        function getPCONTMouseOver(featureSelected)
        {

            var cont = "<table class='table'><tr><td>District</td><td>" + featureSelected.properties.name + "</td></tr><tr><td>Existing Voters</td><td>" + featureSelected.properties.totla_vote + "</td></tr><tr><td>New Female</td><td>" + featureSelected.properties.female_inc + "</td></tr><tr><td>New Female(%)</td><td>" + featureSelected.properties.female_per + "</td></tr><tr><td>New Male</td><td>" + featureSelected.properties.male_inc_n + "</td></tr><tr><td>New Male(%)</td><td>" + featureSelected.properties.male_per + "</td></tr><tr><td>Total New Voters</td><td>" + featureSelected.properties.totla_inc_ + "</td></tr><tr></table>";
            return cont;


        }



        function getPCONT(featureSelected)
        {

            var cont = "<table class='table'><tr><td>"+$translate.instant('District')+"</td><td>" + $translate.instant(featureSelected.properties.name) + "</td></tr><tr><td>"+$translate.instant('Existing Voters')+"</td><td>" +$translate.instant(featureSelected.properties.totla_vote)+"</td></tr><tr><td>"+$translate.instant('New Female')+"</td><td>" +$translate.instant(featureSelected.properties.female_inc)+"</td></tr><tr><td>"+$translate.instant('New Female')+"(%)</td><td>" +$translate.instant(featureSelected.properties.female_per)+"</td></tr><tr><td>"+$translate.instant('New Male')+"</td><td>" +$translate.instant(featureSelected.properties.male_inc_n)+ "</td></tr><tr><td>"+$translate.instant('New Male')+"(%)</td><td>" +$translate.instant(featureSelected.properties.male_per)+ "</td></tr><tr><td>"+$translate.instant('Total New Voters')+"</td><td>" +$translate.instant(featureSelected.properties.totla_inc_)+ "</td></tr><tr></table>";
            return cont;


        }

        angular.extend($scope, {
            bd: {
                lat: 24.1000,
                lng: 90.3500,
                zoom: 7
            },
            defaults: {
                scrollWheelZoom: true,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    img: {
                        name: 'img',

                        url: 'images/be.png',

                        type: 'xyz'
                    },
                    act: {
                        name: 'Acetate',
                        url: 'http://a{s}.acetate.geoiq.com/tiles/terrain/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                },
                //overlays: {
                //    wms: {
                //        name: 'Bangladesh',
                //        type: 'wms',
                //        visible: true,
                //        url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                //        layerParams: {
                //            service: 'WMS',
                //            version: '1.1.0',
                //            request: 'GetMap',
                //            srs: 'EPSG:4326',
                //            style: 'polygon',
                //            layers: 'distr-map-voter-update-data',
                //            format: 'image/png',
                //            transparent: true
                //        }
                //    }
                //}
            }



        });



        function countryMouseover(feature, leafletEvent) {
            var layer = leafletEvent.target;
            layer.setStyle({
                fillColor: 'white'
            });
            layer.bringToFront();
            $scope.name = feature.properties.name;
            $scope.gap = feature.properties.m_f_diff;

            $scope.femper = feature.properties.female_per;

            $scope.maleper = feature.properties.male_per;


            console.log(feature);
        }


        function getColor(code) {

            var nc = Math.floor(code);
            if (nc < 5)
            {
                return "#FE8484";
            }

            if (nc >= 5 && nc < 10)
            {
                return "#FE5C5C";
            }

            if (nc >= 10 && nc < 15)
            {
                return "#FE3939";
            }


            if (nc >= 15 && nc < 20)
            {
                return "#FE2020";
            }

            if (nc >= 20 && nc < 25)
            {
                return "#DB0000";
            }

            if (nc >= 25 && nc < 30)
            {
                return "#BA0000";
            }

            if (nc >= 30 && nc < 35)
            {
                return "#6B0000";
            }

            if (nc >= 35 && nc < 40)
            {
                return "#530000";
            }

            if (nc >= 40)
            {
                return "#250000";
            }
        }

        function resetHighlight(e) {
            geojson.resetStyle(e.target);
        }
        function style(feature)
        {
            return {
                fillColor: getColor(feature.properties.m_f_diff),
                weight: 1,
                opacity: 1,
                color: 'white',
                //  dashArray: '3',
                fillOpacity: 0.7
            };
        }


        $http.get(map_path).success(function (data, status) {
            angular.extend($scope, {
                geojson: {
                    data: data,
                    style: style,
                    mouseout: resetHighlight,
                    resetStyleOnMouseout: true,
                    onEachFeature: collectMe
                }
            });
            $scope.features = data.properties;
        });





        //----modal start-------------------


        $scope.animationsEnabled = true;


        $scope.openModal = function () {

            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'templates/myModalContent.html',
                controller: 'ModalInstanceController'
                //size: size
                //resolve: {
                //    items: function () {
                //        return $scope.items;
                //    }
                //}
            });
        };

        $scope.reccount =0;

        $http.get('signpledge/')
            .success(function(data) {
                console.log(data.length);
                $scope.reccount =data.length;

                $rootScope.recount = data.length;


                //--default start

                $rootScope.langsel = 'bn_sel';
                var rootvar = String($rootScope.recount);


                //$rootScope.recount=rootvar+1;

                //ar str = "HELLO WORLD";
                var arr = [];
                for (var i = 0; i < rootvar.length; i++) {
                    //arr.push(rootvar.charAt(i));
                    //console.log(rootvar.charAt(i));
                    console.log(arr);


                    if (rootvar.charAt(i) == '0') {
                        arr.push('০');
                    }
                    else if (rootvar.charAt(i) == '1') {
                        arr.push('১');
                    }
                    else if (rootvar.charAt(i) == '2') {
                        arr.push('২');
                    }
                    else if (rootvar.charAt(i) == '3') {
                        arr.push('৩');
                    }
                    else if (rootvar.charAt(i) == '4') {
                        arr.push('৪');
                    }
                    else if (rootvar.charAt(i) == '5') {
                        arr.push('৫');
                    }
                    else if (rootvar.charAt(i) == '6') {
                        arr.push('৬');
                    }
                    else if (rootvar.charAt(i) == '7') {
                        arr.push('৭');
                    }
                    else if (rootvar.charAt(i) == '8') {
                        arr.push('৮');
                    }
                    else if (rootvar.charAt(i) == '9') {
                        arr.push('৯');
                    }

                }
                $rootScope.recount=arr.join('');
                //--Default End---





            });


        //---modal end------------------------


        //$scope.eventUpdateLastMessage_cb = function (event, args) {
        //    console.log(args);
        //};

        //$scope.$on("eventLastMessageUpdate", $scope.eventUpdateLastMessage_cb);
        $scope.$on('eventLastMessageUpdate', function(event, args) {
        console.log('hello hello');
            $http.get('signpledge/')
                .success(function(data) {
                    console.log(data.length);
                    $scope.reccount =data.length;

                    $rootScope.recount = data.length;
                });
        });


        $scope.toggle = function() {
            $scope.$broadcast('event:toggle');
        }


    })

    .directive('toggle', function() {
        return function(scope, elem, attrs) {
            scope.$on('event:toggle', function() {
                elem.slideToggle();
            });
        };
    })
;