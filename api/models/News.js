module.exports = {
  attributes: {
    date: {
      type: 'date'
    },
    title:{
        type:'string'
    },
    link:{
        type:'string'
    },
    source: {
      type: 'string'
    },
    tags:{
        type:'string'
    },        
    featured: {
      type: 'boolean'
    },
    order: {
      type: 'integer'
    },
  }
};