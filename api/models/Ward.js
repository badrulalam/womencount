module.exports = {
  attributes: {
    key: {
      type: 'string',
    },
    name:{
        type:'string'
    },
    city:{
        type:'string'
    },
    bangla:{
        type:'string'
    },
    population: {
      type: 'integer'
    },
    voter:{
        type:'integer'
    },
  }
};
