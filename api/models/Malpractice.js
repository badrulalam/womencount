module.exports = {
  attributes: {
    where_picture_taken: {
      type: 'string',
    },
    when_picture_taken: {
      type: 'string'
    },
    details: {
      type: 'string'
    },
    name: {
      type: 'string'
    },    
    phone: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    image: {
      type: 'string'
    },
  },
};