/**
 * SboController
 *
 * @description :: Server-side logic for managing sboes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	dscc : function(req,res){
		var mysql      = require('mysql');
		var connection = mysql.createConnection({
			host     : 'localhost',
			user     : 'root',
			password : 'lamp',
			database : 'dhakasouthcc'

      //this host/droplet name is testface
      //host     : '45.55.232.190',
      //user     : 'root',
      //password : '2JN3nEQZNF',
      //database : 'dhakasouthcc'

      //sbo
      //host     : '128.199.221.179',
      //user     : 'root',
      //password : '5W2PGNujoY',
      //database : 'dhakasouthcc'
		});

		connection.connect();
		var incident = req.query["incident"];
		console.log("dscc" + incident);
    //var querySQL = 'SELECT ps.Serial_No as ps_id, ps.Latitude, ps.Longitude, count(ps.Serial_No) as totalIncidents, ps.PS_Name_In_English as ps_name, sms.question_no as sms_ques_no, sms.ansvalue as sms_ansvalue from smsfromsto sms, stobserber stob, poling_station_coord ps where sms.stobserber_id=stob.id and stob.pollstation_id=ps.Serial_No and sms.question_no=69 and sms.is_correct=1 and sms.ansvalue="'+incident+'" group by ps.Serial_No';

    //var querySQL = 'SELECT pol.id as ps_id, ps.Latitude, ps.Longitude, count(pol.id) as totalIncidents, pol.name as ps_name, sms.question_no as sms_ques_no, sms.ansvalue as sms_ansvalue from smsfromsto sms, stobserber stob, poling_station_coord ps, pollstation pol where sms.stobserber_id=stob.id and stob.pollstation_id=ps.Serial_No and ps.Serial_No=pol.ze_id and sms.question_no=69 and sms.is_correct=1 and sms.ansvalue="'+incident+'" group by pol.id';

    //var querySQL = 'SELECT wc.ward_no, wc.Latitude, wc.Longitude, sms.question_no as sms_ques_no,sms.ansvalue as sms_ansvalue, count(sms.ansvalue) as totalIncidents from smsfromsto sms, ward_coord wc where sms.ansvalue="'+incident+'" and sms.ward_id=wc.ward_no and sms.question_no=99 and sms.is_correct=1 group by wc.ward_no,sms.question_no, sms.ansvalue';
		var querySQL = 'SELECT     stobserber.pollstation_id,      poling_station_coord.Latitude,      poling_station_coord.Longitude,      smsfromsto.stquestionans_id,      smsfromsto.question_no,      smsfromsto.ans_no,      Count(smsfromsto.ansvalue) as totalIncidents    FROM    stobserber    INNER JOIN smsfromsto ON stobserber.id = smsfromsto.stobserber_id    INNER JOIN poling_station_coord ON poling_station_coord.Serial_No = stobserber.pollstation_id    WHERE    smsfromsto.is_correct = 1 AND smsfromsto.ansvalue="'+incident+'" AND   smsfromsto.question_no = 99    GROUP BY    stobserber.pollstation_id';

		connection.query(querySQL, function(err, rows, fields) {
			if (!err)
				res.json(rows);
			else
			res.json(err);

		});
		connection.end();
	},
  dsccall : function(req,res){
    var mysql      = require('mysql');
    var connection = mysql.createConnection({
      host     : 'localhost',
      user     : 'root',
      password : 'lamp',
      database : 'dhakasouthcc'


      //host     : '128.199.221.179',
      //user     : 'root',
      //password : '5W2PGNujoY',
      //database : 'dhakasouthcc'


      //this host/droplet name is testface
      //host     : '45.55.232.190',
      //user     : 'root',
      //password : '2JN3nEQZNF',
      //database : 'dhakasouthcc'
    });

    connection.connect();
    var incident = req.query["incident"];
    console.log("dscc" + incident);
    //var querySQL = 'SELECT wc.ward_no, wc.Latitude, wc.Longitude, sms.question_no as sms_ques_no,sms.ansvalue as sms_ansvalue, count(sms.ansvalue) as totalIncidents from smsfromsto sms, ward_coord wc where  sms.ward_id=wc.ward_no and sms.question_no=99 and sms.is_correct=1 group by wc.ward_no,sms.question_no, sms.ansvalue';
    var querySQL = "SELECT     stobserber.pollstation_id,      poling_station_coord.Latitude,      poling_station_coord.Longitude,      smsfromsto.stquestionans_id,      smsfromsto.question_no,      smsfromsto.ans_no,      Count(smsfromsto.ansvalue) as totalIncidents    FROM    stobserber    INNER JOIN smsfromsto ON stobserber.id = smsfromsto.stobserber_id    INNER JOIN poling_station_coord ON poling_station_coord.Serial_No = stobserber.pollstation_id    WHERE    smsfromsto.is_correct = 1 AND    smsfromsto.question_no = 99    GROUP BY    stobserber.pollstation_id";
    connection.query(querySQL, function(err, rows, fields) {
      if (!err)
        res.json(rows);
      else
        res.json(err);

    });
    connection.end();
  },
	dncc : function(req,res){
		var mysql      = require('mysql');
		var connection = mysql.createConnection({
			host     : 'localhost',
			user     : 'root',
			password : 'lamp',
			database : 'dhakanorthcc'

      //this host/droplet name is testface
      //host     : '45.55.232.190',
      //user     : 'root',
      //password : '2JN3nEQZNF',
      //database : 'dhakanorthcc'

      //sbo
      //host     : '128.199.221.179',
      //user     : 'root',
      //password : '5W2PGNujoY',
      //database : 'dhakanorthcc'
		});

		connection.connect();
		var incident = req.query["incident"];
    console.log("dscc" + incident);
    //var querySQL = 'SELECT ps.Serial_No as ps_id, ps.Latitude, ps.Longitude, count(ps.Serial_No) as totalIncidents, ps.PS_Name_In_English as ps_name, sms.question_no as sms_ques_no, sms.ansvalue as sms_ansvalue from smsfromsto sms, stobserber stob, poling_station_coord ps where sms.stobserber_id=stob.id and stob.pollstation_id=ps.Serial_No and sms.question_no=69 and sms.is_correct=1 and sms.ansvalue="'+incident+'" group by ps.Serial_No';

    //var querySQL = 'SELECT pol.id as ps_id, ps.Latitude, ps.Longitude, count(pol.id) as totalIncidents, pol.name as ps_name, sms.question_no as sms_ques_no, sms.ansvalue as sms_ansvalue from smsfromsto sms, stobserber stob, poling_station_coord ps, pollstation pol where sms.stobserber_id=stob.id and stob.pollstation_id=ps.Serial_No and ps.Serial_No=pol.ze_id and sms.question_no=69 and sms.is_correct=1 and sms.ansvalue="'+incident+'" group by pol.id';

    //var querySQL = 'SELECT wc.ward_no, wc.Latitude, wc.Longitude, sms.question_no as sms_ques_no,sms.ansvalue as sms_ansvalue, count(sms.ansvalue) as totalIncidents from smsfromsto sms, ward_coord wc where sms.ansvalue="'+incident+'" and sms.ward_id=wc.ward_no and sms.question_no=99 and sms.is_correct=1 group by wc.ward_no,sms.question_no, sms.ansvalue';
		var querySQL = 'SELECT     stobserber.pollstation_id,      poling_station_coord.Latitude,      poling_station_coord.Longitude,      smsfromsto.stquestionans_id,      smsfromsto.question_no,      smsfromsto.ans_no,      Count(smsfromsto.ansvalue) as totalIncidents    FROM    stobserber    INNER JOIN smsfromsto ON stobserber.id = smsfromsto.stobserber_id    INNER JOIN poling_station_coord ON poling_station_coord.Serial_No = stobserber.pollstation_id    WHERE    smsfromsto.is_correct = 1 AND smsfromsto.ansvalue="'+incident+'" AND   smsfromsto.question_no = 99    GROUP BY    stobserber.pollstation_id';

    connection.query(querySQL, function(err, rows, fields) {
			if (!err)
				res.json(rows);
			else
			res.json(err);

		});
		connection.end();
	},
  dnccall : function(req,res){
    var mysql      = require('mysql');
    var connection = mysql.createConnection({

      host     : 'localhost',
      user     : 'root',
      password : 'lamp',
      database : 'dhakanorthcc'
      //sbo
      //host     : '128.199.221.179',
      //user     : 'root',
      //password : '5W2PGNujoY',
      //database : 'dhakanorthcc'


      //this host/droplet name is testface
      //host     : '45.55.232.190',
      //user     : 'root',
      //password : '2JN3nEQZNF',
      //database : 'dhakanorthcc'

    });

    connection.connect();
    var incident = req.query["incident"];
    console.log("dscc" + incident);

    //var querySQL = 'SELECT wc.ward_no, wc.Latitude, wc.Longitude, sms.question_no as sms_ques_no,sms.ansvalue as sms_ansvalue, count(sms.ansvalue) as totalIncidents from smsfromsto sms, ward_coord wc where  sms.ward_id=wc.ward_no and sms.question_no=99 and sms.is_correct=1 group by wc.ward_no,sms.question_no, sms.ansvalue';
    var querySQL = "SELECT     stobserber.pollstation_id,      poling_station_coord.Latitude,      poling_station_coord.Longitude,      smsfromsto.stquestionans_id,      smsfromsto.question_no,      smsfromsto.ans_no,      Count(smsfromsto.ansvalue) as totalIncidents    FROM    stobserber    INNER JOIN smsfromsto ON stobserber.id = smsfromsto.stobserber_id    INNER JOIN poling_station_coord ON poling_station_coord.Serial_No = stobserber.pollstation_id    WHERE    smsfromsto.is_correct = 1 AND    smsfromsto.question_no = 99    GROUP BY    stobserber.pollstation_id";
    connection.query(querySQL, function(err, rows, fields) {
      if (!err)
        res.json(rows);
      else
        res.json(err);

    });
    connection.end();
  },
	ccc : function(req,res){
		var mysql      = require('mysql');
		var connection = mysql.createConnection({
			host     : 'localhost',
			user     : 'root',
			password : 'lamp',
			database : 'chittagongcc'

      //this host/droplet name is testface
      //host     : '45.55.232.190',
      //user     : 'root',
      //password : '2JN3nEQZNF',
      //database : 'chittagongcc'

      //sbo
      //host     : '128.199.221.179',
      //user     : 'root',
      //password : '5W2PGNujoY',
      //database : 'chittagongcc'
		});

		connection.connect();
		var incident = req.query["incident"];
    console.log("ccc" + incident);
    //var querySQL = 'SELECT ps.Serial_No as ps_id, ps.Latitude, ps.Longitude, count(ps.Serial_No) as totalIncidents, ps.PS_Name_In_English as ps_name, sms.question_no as sms_ques_no, sms.ansvalue as sms_ansvalue from smsfromsto sms, stobserber stob, poling_station_coord ps where sms.stobserber_id=stob.id and stob.pollstation_id=ps.Serial_No and sms.question_no=69 and sms.is_correct=1 and sms.ansvalue="'+incident+'" group by ps.Serial_No';

    //SELECT ps.Serial_No as ps_id, ps.Latitude, ps.Longitude, count(ps.Serial_No) as totalIncidents, ps.PS_Name_In_English as ps_name, sms.question_no as sms_ques_no, sms.ansvalue as sms_ansvalue from smsfromsto sms, stobserber stob, poling_station_coord ps where sms.stobserber_id=stob.id and stob.pollstation_id=ps.Serial_No and sms.question_no=69 and sms.is_correct=1 and sms.ansvalue="'+incident+'" group by ps.Serial_No

    //var querySQL = 'SELECT pol.id as ps_id, ps.Latitude, ps.Longitude, count(pol.id) as totalIncidents, pol.name as ps_name, sms.question_no as sms_ques_no, sms.ansvalue as sms_ansvalue from smsfromsto sms, stobserber stob, poling_station_coord ps, pollstation pol where sms.stobserber_id=stob.id and stob.pollstation_id=ps.Serial_No and ps.Serial_No=pol.ze_id and sms.question_no=69 and sms.is_correct=1 and sms.ansvalue="'+incident+'" group by pol.id';

		//var querySQL = 'SELECT wc.ward_no, wc.Latitude, wc.Longitude, sms.question_no as sms_ques_no,sms.ansvalue as sms_ansvalue, count(sms.ansvalue) as totalIncidents from smsfromsto sms, ward_coord wc where sms.ansvalue="'+incident+'" and sms.ward_id=wc.ward_no and sms.question_no=99 and sms.is_correct=1 group by wc.ward_no,sms.question_no, sms.ansvalue';
		var querySQL = 'SELECT     stobserber.pollstation_id,      poling_station_coord.Latitude,      poling_station_coord.Longitude,      smsfromsto.stquestionans_id,      smsfromsto.question_no,      smsfromsto.ans_no,      Count(smsfromsto.ansvalue) as totalIncidents    FROM    stobserber    INNER JOIN smsfromsto ON stobserber.id = smsfromsto.stobserber_id    INNER JOIN poling_station_coord ON poling_station_coord.Serial_No = stobserber.pollstation_id    WHERE    smsfromsto.is_correct = 1 AND smsfromsto.ansvalue="'+incident+'" AND   smsfromsto.question_no = 99    GROUP BY    stobserber.pollstation_id';

    connection.query(querySQL, function(err, rows, fields) {
			if (!err)
				res.json(rows);
			else
			res.json(err);

		});
		connection.end();
	},
  cccall : function(req,res){
    var mysql      = require('mysql');
    var connection = mysql.createConnection({
      host     : 'localhost',
      user     : 'root',
      password : 'lamp',
      database : 'chittagongcc'

      //this host/droplet name is testface
      //host     : '45.55.232.190',
      //user     : 'root',
      //password : '2JN3nEQZNF',
      //database : 'chittagongcc'

      //sbo
      //host     : '128.199.221.179',
      //user     : 'root',
      //password : '5W2PGNujoY',
      //database : 'chittagongcc'
    });

    connection.connect();
    var incident = req.query["incident"];
    console.log("ccc" + incident);


   // var querySQL = 'SELECT wc.ward_no, wc.Latitude, wc.Longitude, sms.question_no as sms_ques_no,sms.ansvalue as sms_ansvalue, count(sms.ansvalue) as totalIncidents from smsfromsto sms, ward_coord wc where  sms.ward_id=wc.ward_no and sms.question_no=99 and sms.is_correct=1 group by wc.ward_no,sms.question_no, sms.ansvalue';
    var querySQL = "SELECT     stobserber.pollstation_id,      poling_station_coord.Latitude,      poling_station_coord.Longitude,      smsfromsto.stquestionans_id,      smsfromsto.question_no,      smsfromsto.ans_no,      Count(smsfromsto.ansvalue) as totalIncidents    FROM    stobserber    INNER JOIN smsfromsto ON stobserber.id = smsfromsto.stobserber_id    INNER JOIN poling_station_coord ON poling_station_coord.Serial_No = stobserber.pollstation_id    WHERE    smsfromsto.is_correct = 1 AND    smsfromsto.question_no = 99    GROUP BY    stobserber.pollstation_id";
    connection.query(querySQL, function(err, rows, fields) {
      if (!err)
        res.json(rows);
      else
        res.json(err);

    });
    connection.end();
  }

};

//CREATE TABLE `dhakanorthcc`.`ward_coord` ( `Longitude` VARCHAR(20) NOT NULL , `Latitude` VARCHAR(20) NOT NULL , `ward_no` VARCHAR(10) NOT NULL ) ENGINE = InnoDB

//nano /etc/mysql/my.cnf
// bind-address    = 127.0.0.1 to
// replace with below if we want to permit remote ip
// bind-address    = 0.0.0.0



//see mysql with tcp is permitted for hosts
//lsof -i -P | grep :3306
//mysqld  3106    mysql   10u  IPv4  15831      0t0  TCP *:3306 (LISTEN)

//impose all previlege for remote host
//GRANT ALL PRIVILEGES ON dhakasouthcc.* To 'root'@'182.48.95.42' IDENTIFIED BY '5W2PGNujoY';
//GRANT ALL PRIVILEGES ON dhakanorthcc.* To 'root'@'182.48.95.42' IDENTIFIED BY '5W2PGNujoY';
//GRANT ALL PRIVILEGES ON chittagongcc.* To 'root'@'182.48.95.42' IDENTIFIED BY '5W2PGNujoY';


//GRANT ALL PRIVILEGES ON dhakanorthcc.* To 'root'@'202.51.183.38' IDENTIFIED BY '2JN3nEQZNF';
//GRANT ALL PRIVILEGES ON chittagongcc.* To 'root'@'202.51.183.38' IDENTIFIED BY '2JN3nEQZNF';



//GRANT ALL PRIVILEGES ON dhakasouthcc.* To 'root'@'202.51.183.38' IDENTIFIED BY '5W2PGNujoY';
//GRANT ALL PRIVILEGES ON dhakanorthcc.* To 'root'@'202.51.183.38' IDENTIFIED BY '5W2PGNujoY';
//GRANT ALL PRIVILEGES ON chittagongcc.* To 'root'@'202.51.183.38' IDENTIFIED BY '5W2PGNujoY';


//sbo droplet previlizing citycorpelections.org droplet
//GRANT ALL PRIVILEGES ON dhakasouthcc.* To 'root'@'128.199.81.150' IDENTIFIED BY '5W2PGNujoY';
//GRANT ALL PRIVILEGES ON dhakanorthcc.* To 'root'@'128.199.81.150' IDENTIFIED BY '5W2PGNujoY';
//GRANT ALL PRIVILEGES ON chittagongcc.* To 'root'@'128.199.81.150' IDENTIFIED BY '5W2PGNujoY';


//for restart mysql
//sudo service mysql restart


// all here
//http://stackoverflow.com/questions/15663001/remote-connections-mysql-ubuntu
