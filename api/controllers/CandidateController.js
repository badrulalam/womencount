module.exports = {

  getCandidatesByLimit : function(req,res){
    //console.log("ekhane aiseto");
    //console.log(req.query);

    Candidate.find()
      .limit(req.query["limit"])
      .sort({ id: req.query["order"] })
      .exec(function(err, data){
        if (err) return next(err);
        res.json(data);
      });
  },
  getTotalCandidates : function(req,res){
    Candidate.count().exec(function countCB(error, totalCandidates) {
      console.log('Total ' + totalCandidates + ' ');
      res.json(totalCandidates);
    });
  },
  getTotalVote : function(req,res){

    Candidate.find()
    .where({ city: req.query["city"] , type: 'mayor' } )
    .sum('vote_received')
    .exec(function countCB(error, totalCandidates) {
      res.json(totalCandidates);
    });
  },
  getTotalTypeCandidates : function(req,res){
    console.log(req.query["city"]);

    Candidate.count()
      .where({ city: req.query["city"] })
      .exec(function countCB(error, totalCandidates) {
      console.log('Total ' + totalCandidates + ' ');
      res.json(totalCandidates);
    });
  }
};
